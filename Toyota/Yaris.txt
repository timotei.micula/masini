The Toyota Yaris (Japanese: ??? �??? Toyota Yarisu) is a subcompact car sold by Toyota since 1999, replacing the Starlet.

Between 1999 and 2005, some markets received the same vehicles under the Toyota Echo name. Toyota has used the "Yaris" and "Echo" names on the export version of several different Japanese-market models. From 2015, most Yaris sedan models marketed in North America are based on the Mazda2 and produced for Toyota by Mazda.

The name "Yaris" is derived from "Charis", the singular form of Charites, the Greek goddesses 
of charm and beauty.[1] For the fourth generation model, the name "Yaris" will be used worldwide,
 including Japan (replaces the "Vitz" name